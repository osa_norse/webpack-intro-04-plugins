module.exports = {
    entry: './app.js',
    output: {
        filename: './build/bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    }
};
