import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.css';

$('#load-button').click(() => {
    $.get('./dev.json').then((data) => {
        $('#output').html(JSON.stringify(data, null, 2));
    });
});